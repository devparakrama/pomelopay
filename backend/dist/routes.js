"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const posts_controller_1 = require("./controllers/posts/posts.controller");
const home_controller_1 = require("./controllers/home/home.controller");
class Routes {
    constructor() {
        this.router = express.Router();
        this.homeCon = new home_controller_1.default();
    }
    initiate() {
        return [
            new home_controller_1.default(),
            new posts_controller_1.default()
        ];
    }
}
exports.default = Routes;
//# sourceMappingURL=routes.js.map