"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = require("./app");
const routes_1 = require("./routes");
const bodyParser = require("body-parser");
const dotenv = require("dotenv");
const logger_1 = require("./middleware/logger");
dotenv.config();
const routes = new routes_1.default;
const app = new app_1.default({
    port: process.env.PORT,
    controllers: routes.initiate(),
    middleWares: [
        bodyParser.json(),
        bodyParser.urlencoded({ extended: true }),
        logger_1.default
    ]
});
app.listen();
//# sourceMappingURL=server.js.map