"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const transactionModel_1 = require("../../models/transactionModel");
class HomeController {
    constructor() {
        this.path = '/';
        this.router = express.Router();
        this.transactions = new transactionModel_1.default();
        this.index = (req, res) => {
            this.transactions.transactions(res);
            //res.send( )
        };
        this.initRoutes();
    }
    initRoutes() {
        this.router.get('/', this.index);
    }
}
exports.default = HomeController;
//# sourceMappingURL=home.controller.js.map