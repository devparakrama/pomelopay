"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const data = require('../../Data/mock.json');
class transactionModel {
    constructor() {
        this.transactions = (res) => {
            const transactionData = data.transactions;
            res.status(200).json({ transactionData });
        };
    }
}
exports.default = transactionModel;
//# sourceMappingURL=transactionModel.js.map