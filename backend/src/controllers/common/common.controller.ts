import * as express from 'express'
import { Request, Response } from 'express'
import IControllerBase from 'interfaces/IControllerBase.interface'
import transactionModel from '../../models/transactionModel';
import { Server } from 'tls';

class CommonController implements IControllerBase {
    
    public path = '/'
    public router = express.Router()
    transactions = new transactionModel();

    constructor() {
        this.initRoutes()
    }

    public initRoutes() {
        
    }
 
}

export default CommonController
