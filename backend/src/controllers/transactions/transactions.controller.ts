import * as express from 'express'
import { Request, Response } from 'express'
import IControllerBase from 'interfaces/IControllerBase.interface'
import transactionModel from '../../models/transactionModel';

class TransactionsController implements IControllerBase {
    
    public path = '/transactions'
    public router = express.Router()
    transactions = new transactionModel();

    constructor() {
        this.initRoutes()
    }

    public initRoutes() {

        this.router.get(this.path, this.index)
        this.router.post(this.path, this.postData)

    }

    index = (req: Request, res: Response) => {
        
        this.transactions.transactions(req, res);
       
    }

    postData = (req: Request, res: Response) => {

         this.transactions.updateTransaction(req,res)
           
    }
    
}

export default TransactionsController
