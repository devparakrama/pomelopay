import App from './app'

import Routes from './routes'

import * as bodyParser from 'body-parser'

import * as dotenv from "dotenv"

import loggerMiddleware from './middleware/logger'

dotenv.config();

const routes = new Routes;

//Passing parameters into the App 
const app = new App({
    port: process.env.PORT,
    controllers: routes.initiate(),
    middleWares: [
        bodyParser.json(),
        bodyParser.urlencoded({ extended: true }),
        loggerMiddleware
    ]
})

app.listen()

