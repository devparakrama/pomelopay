import * as express from 'express'
import loggerMiddleware from './middleware/logger'

import HomeController from './controllers/home/home.controller'
import CommonController from './controllers/common/common.controller'
import TransactionsController from './controllers/transactions/transactions.controller'
class Routes {

    public router = express.Router()
    public homeCon = new HomeController()

    constructor(){}

    public initiate(){

       //Load controllers
       return [
            new HomeController(),
            new CommonController(),
            new TransactionsController()
        ]

    }
 
}


export default Routes