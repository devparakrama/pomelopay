import * as express from 'express'
import * as bodyParser from "body-parser";
import { Application } from 'express'


class App {
    public app: Application
    public port: string

    constructor(appInit: { port: string; middleWares: any; controllers: any; }) {

        this.app = express()

        //Allow cors
        this.app.use(function(req, res, next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            next();
          });
        
        //Load the env data 
        const environment = process.env.NODE_ENV; // development
        const stage = require('./config')[environment];

        //Here we are configuring express to use body-parser as middle-ware.
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(bodyParser.json());

        //Port Asigning
        this.port = appInit.port

        //Initiating middlewares
        this.middleware(appInit.middleWares)

        //Initiating Routes
        this.routes(appInit.controllers)

        //Initiating Assets
        this.assets()
        
    }

    private middleware(middleWares: { forEach: (arg0: (middleWare: any) => void) => void; }) {

        //Initiating middlewares into express
        middleWares.forEach(middleWare => {
            this.app.use(middleWare)
        })

    }

    private routes(controllers: { forEach: (arg0: (controller: any) => void) => void; }) {

         //Initiating Controllers
        controllers.forEach(controller => {
            this.app.use('/', controller.router)
        })

    }

    private assets() {

        //Loading assets
        this.app.use(express.static('public'))

    }
 
    public listen() {

        //Listing to port 
        this.app.listen(this.port, () => {
            console.log(`App listening on the http://localhost:${this.port}`)
        })

    }
}

export default App