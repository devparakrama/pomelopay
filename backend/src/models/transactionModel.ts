import * as data from '../../Data/mock.json';
import ITransactions from '../models/transactions.interface';

class transactionModel{

    transactionRequest = {};
    transactionData = []

    private posts: ITransactions[][] = [data.transactions]

    transactions = ( req, res) => {
      
      //Get json data
      this.transactionData = data.transactions;
      res.send({status: 'success','data' : this.transactionData });
        
    }

    updateTransaction = (req , res) =>{

     //Update transaction
     this.transactionRequest=req.body;
     res.send({ status: 'success', 'Info' : 'Successfully Updated'});

    }

}

export default  transactionModel