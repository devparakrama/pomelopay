interface ITransactions {
    transaction_id: string,
    transaction_type: string,
    transaction_dest: string,
    transaction_uuid: string,
    transaction_status: string,
    transaction_date: string,
    currency:string
}

export default ITransactions