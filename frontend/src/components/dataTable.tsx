import React from 'react';
import MaterialTable, { Column } from 'material-table';
import {default as Data}  from '../../Data/mock.json';

interface Row {
  transaction_id: string;
  transaction_type: string;
  transaction_dest: string;
  transaction_uuid: string;
  transaction_status: string;
  transaction_date: string;
  currency: string;

}
 
interface TableState {
  columns: Array<Column<Row>>;
  data: Row[];
}

export default function DataTable() {

  //Set data table
  const [state, setState] = React.useState<TableState>({
    columns: [
      { title: 'Transaction Id', field: 'transaction_id' },
      { title: 'Transaction Type', field: 'transaction_type' },
      { title: 'Transaction Dest', field: 'transaction_dest' },
      { title: 'Transaction UUID Dest', field: 'transaction_uuid' },
      { title: 'Transactions Status', field: 'transaction_status' },
      { title: 'Transaction Date', field: 'transaction_date' },
      { title: 'Currency', field: 'currency' },

    ],
    data: Data.transactions,
  });
  
 

  return (
    <MaterialTable
      title="Transactions"
      columns={state.columns}
      data={state.data}
      editable={{
        onRowAdd: newData =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              setState((prevState :any) => {
                const data = [...prevState.data];
                data.push(newData);
                return { ...prevState, data };
              });
            }, 600);
          }),
        onRowUpdate: (newData, oldData) =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              if (oldData) {
                setState((prevState :any) => {
                  const data = [...prevState.data];
                  data[data.indexOf(oldData)] = newData;
                  return { ...prevState, data };
                });
              }
            }, 600);
          }),
        onRowDelete: oldData =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              setState((prevState :any) => {
                const data = [...prevState.data];
                data.splice(data.indexOf(oldData), 1);
                return { ...prevState, data };
              });
            }, 600);
          }),
      }}
    />
  );
}
 