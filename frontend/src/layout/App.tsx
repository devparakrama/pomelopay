import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import HeaderMenu from './header'
import { TransactionList } from '../components/transactionsList';

import '../styles/index.css';

class App extends React.PureComponent {
	render() {
		return (
			<React.Fragment>
			<CssBaseline />
			<Container maxWidth="lg">
			<HeaderMenu></HeaderMenu>
			</Container>
			
			<Container maxWidth="lg">
				<TransactionList></TransactionList>
				<Typography component="div"  />
			</Container>
			</React.Fragment>
		);
	}
}

export default App;