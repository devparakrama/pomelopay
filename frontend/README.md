To run in development mode

```bash
npm run start
```

To build

```bash
npm run build
```